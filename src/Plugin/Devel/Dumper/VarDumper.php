<?php

namespace Drupal\devel_vardumper\Plugin\Devel\Dumper;

use Drupal\devel\DevelDumperBase;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;

/**
 * Class VarDumper
 *
 * @DevelDumper(
 *   id = "vardumper",
 *   label = @Translation("Symfony var-dumper"),
 *   description = @Translation("Wrapper for Symfony var-dumper debugging tool."),
 * )
 *
 */
class VarDumper extends DevelDumperBase {

  /**
   * {@inheritdoc}
   */
  public function dump($input, $name = NULL) {
    $cloner = $this->getVarCloner();
    $dumper = 'cli' === PHP_SAPI ? new CliDumper() : \drupal::service('drupal_html_dumper');

    $dumper->dump($cloner->cloneVar($input));
  }

  /**
   * {@inheritdoc}
   */
  public function export($input, $name = NULL) {
    $cloner = $this->getVarCloner();
    $dumper = 'cli' === PHP_SAPI ? new CliDumper() : \drupal::service('drupal_html_dumper');

    $output = fopen('php://memory', 'r+b');
    $dumper->dump($cloner->cloneVar($input), $output);
    $output = stream_get_contents($output, -1, 0);

    return $this->setSafeMarkup($output);
  }

  /**
   * {@inheritdoc}
   */
  public static function checkRequirements() {
    return class_exists('Symfony\Component\VarDumper\Cloner\VarCloner', TRUE);
  }

  /**
   * @return \Symfony\Component\VarDumper\Cloner\VarCloner
   */
  private function getVarCloner() {
    $cloner = new VarCloner();
    $myCasters = [
      'Drupal\Core\Session\UserSession' => 'Drupal\devel_vardumper\Caster\DrupalCaster::castUser',
    ];
    $cloner->addCasters($myCasters);
    return $cloner;
  }
}
