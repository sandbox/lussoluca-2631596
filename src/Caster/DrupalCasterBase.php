<?php

namespace Drupal\devel_vardumper\Caster;

/**
 * Class DrupalCasterBase
 */
class DrupalCasterBase {

  protected static function generateLink($url, $name) {
    return "link:$url|$name";
  }
}
