<?php

namespace Drupal\devel_vardumper\Dumper;

use Drupal\webprofiler\Helper\ClassShortenerInterface;
use Drupal\webprofiler\Helper\IdeLinkGeneratorInterface;
use Symfony\Component\VarDumper\Cloner\Cursor;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;

/**
 * HtmlDumper dumps variables as HTML.
 */
class DrupalHtmlDumper extends HtmlDumper {

  /**
   * @var \Drupal\webprofiler\Helper\ClassShortenerInterface
   */
  private $classShortener;

  /**
   * @var \Drupal\webprofiler\Helper\IdeLinkGeneratorInterface
   */
  private $ideLinkGenerator;

  /**
   * {@inheritdoc}
   */
  protected $styles = array(
    'default' => 'background-color:#18171B; color:#FF8400; line-height:1.2em; font:12px Menlo, Monaco, Consolas, monospace; word-wrap: break-word; white-space: pre-wrap; position:relative; z-index:10; word-break: normal',
    'num' => 'font-weight:bold; color:#1299DA',
    'const' => 'font-weight:bold',
    'str' => 'font-weight:bold; color:#56DB3A',
    'note' => 'color:#1299DA',
    'link' => 'color:#1299DA',
    'ref' => 'color:#A0A0A0',
    'public' => 'color:#FFFFFF',
    'protected' => 'color:#FFFFFF',
    'private' => 'color:#FFFFFF',
    'meta' => 'color:#B729D9',
    'key' => 'color:#56DB3A',
    'index' => 'color:#1299DA',
  );

  /**
   * @param \Drupal\webprofiler\Helper\IdeLinkGeneratorInterface $ideLinkGenerator
   * @param \Drupal\webprofiler\Helper\ClassShortenerInterface $classShortener
   */
  public function __construct(
    IdeLinkGeneratorInterface $ideLinkGenerator = NULL,
    ClassShortenerInterface $classShortener = NULL
  ) {
    parent::__construct();

    $this->ideLinkGenerator = $ideLinkGenerator;
    $this->classShortener = $classShortener;
  }

  /**
   * {@inheritdoc}
   */
  public function dumpString(Cursor $cursor, $str, $bin, $cut) {
    $this->dumpKey($cursor);

    if ($bin) {
      $str = $this->utf8Encode($str);
    }
    if ('' === $str) {
      $this->line .= '""';
      $this->dumpLine($cursor->depth, TRUE);
    }
    else {
      $attr = array(
        'length' => 0 <= $cut && function_exists('iconv_strlen') ? iconv_strlen(
            $str,
            'UTF-8'
          ) + $cut : 0,
        'binary' => $bin,
      );
      $str = explode("\n", $str);
      if (isset($str[1]) && !isset($str[2]) && !isset($str[1][0])) {
        unset($str[1]);
        $str[0] .= "\n";
      }
      $m = count($str) - 1;
      $i = $lineCut = 0;

      if ($bin) {
        $this->line .= 'b';
      }

      if ($m) {
        $this->line .= '"""';
        $this->dumpLine($cursor->depth);
      }
      else {
        $this->line .= '"';
      }

      foreach ($str as $str) {
        if ($i < $m) {
          $str .= "\n";
        }
        if (0 < $this->maxStringWidth && $this->maxStringWidth < $len = iconv_strlen(
            $str,
            'UTF-8'
          )
        ) {
          $str = iconv_substr($str, 0, $this->maxStringWidth, 'UTF-8');
          $lineCut = $len - $this->maxStringWidth;
        }
        if ($m && 0 < $cursor->depth) {
          $this->line .= $this->indentPad;
        }
        if ('' !== $str) {
          if ("link:" === substr($str, 0, 5)) {
            $this->line .= $this->style('link', $str, $attr);
          }
          else {
            $this->line .= $this->style('str', $str, $attr);
          }
        }
        if ($i++ == $m) {
          if ($m) {
            if ('' !== $str) {
              $this->dumpLine($cursor->depth);
              if (0 < $cursor->depth) {
                $this->line .= $this->indentPad;
              }
            }
            $this->line .= '"""';
          }
          else {
            $this->line .= '"';
          }
          if ($cut < 0) {
            $this->line .= '…';
            $lineCut = 0;
          }
          elseif ($cut) {
            $lineCut += $cut;
          }
        }
        if ($lineCut) {
          $this->line .= '…' . $lineCut;
          $lineCut = 0;
        }

        $this->dumpLine($cursor->depth, $i > $m);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function style($style, $value, $attr = array()) {
    if ('' === $value) {
      return '';
    }

    $v = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
    if ('note' === $style && FALSE !== $c = strrpos($v, '\\')) {
      return $this->getClassStyle($style, $v, $c);
    }

    if ('link' === $style) {
      $parts = explode('|', $value);
      return sprintf('<a href=%s>%s</a>', substr($parts[0], 6), $parts[1]);
    }

    return parent::style($style, $value, $attr);
  }

  /**
   * @param $style
   * @param $v
   * @param $c
   *
   * @return string
   */
  protected function getClassStyle($style, $v, $c) {
    if ($this->ideLinkGenerator && $this->classShortener) {
      $reflectedClass = new \ReflectionClass($v);
      $file = $reflectedClass->getFileName();

      $ideLink = $this->ideLinkGenerator->generateLink($file, 0);
      $abbr = $this->classShortener->shortenClass($v, 'sf-dump-' . $style);

      return sprintf('<a href=%s>%s</a>', $ideLink, $abbr);
    }
    else {
      return sprintf(
        '<abbr title="%s" class=sf-dump-%s>%s</abbr>',
        $v,
        $style,
        substr($v, $c + 1)
      );
    }
  }
}
